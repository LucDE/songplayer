function dom_id(id) {
  return document.getElementById(id);
}
function dom_id_value(id) {
  return document.getElementById(id).value;
}
function removeClassList(id, nameOfClass) {
  return dom_id(id).classList.remove(nameOfClass);
}
function addClassName(id, nameOfClass) {
  return dom_id(id).classList.add(nameOfClass);
}
function changeClassName(id, nameOfClass) {
  dom_id(id).className = nameOfClass;
}
