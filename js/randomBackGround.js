function populate() {
  var hex = [
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "a",
    "b",
    "c",
    "d",
    "e",
  ];
  var colorRandom = "#";
  for (var i = 0; i < 6; i++) {
    var x = Math.round(Math.random() * 14);
    var y = hex[x];
    colorRandom += y;
  }
  return colorRandom;
}

function randomBackgroundColor() {
  var colorOne = populate();
  var colorTwo = populate();
  document.body.style.background =
    "linear-gradient(to right," + colorOne + "," + colorTwo + ")";
  document.querySelector(".slider").style.background =
    "linear-gradient(to right," + colorOne + "," + colorTwo + ")";
}
