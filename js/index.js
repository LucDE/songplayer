var count = 0;
var isShuffle = false;
const audio = dom_id("audio");
const audio_seri = dom_id("seri");
const audio_name = dom_id("name");
const artist_name = dom_id("artist");
const audio_path = dom_id("audio");
const audio_image = dom_id("img");
const full_time_text = dom_id("full_time");
const played_time_text = dom_id("played_time");
const Album = {
  seri: ["1", "2", "3", "4", "5"],
  name: ["Faded", "Đào nương", "Deja vu", "Mộ tuyết", "Xuân Tháng Ba"],
  artist: [
    "Alan Walker",
    "Hoàng Vương",
    "Olivia Rodrigo",
    "Thiên Sơn Mộ Tuyết OST",
    "TunamTina",
  ],
  path: [
    "./songs/Alan Walker - Faded.mp3",
    "./songs/DaoNuong-HoangVuong-7037330.mp3",
    "./songs/Deja Vu - Olivia Rodrigo.mp3",
    "./songs/MoTuyetThienSonMoTuyetOST-V.A-2982516.mp3",
    "./songs/XuanThangBa-TuNamTiNa-6231142.mp3",
  ],
  image: [
    "https://cyber-music-player.vercel.app/images/Faded.jpg",
    "https://i.ytimg.com/vi/PM9Vc_agAqg/maxresdefault.jpg",
    "https://i1.sndcdn.com/artworks-LvTdHlotpRViB8DK-yIyxcw-t500x500.jpg",
    "https://salt.tikicdn.com/ts/tmp/d8/46/10/a4f728d583ca7693589375babeb07c9b.jpg",
    "https://i1.sndcdn.com/artworks-YIesfXSZxuthUthD-2nO8eQ-t500x500.jpg",
  ],
};

function playAudio() {
  addClassName("play", "d-none");
  removeClassList("pause", "d-none");
  changeClassName("img", "img_play_animation");
  removeClassList("wave_animation", "opacity-0");
  audio.play();
}

function pauseAudio() {
  addClassName("pause", "d-none");
  removeClassList("play", "d-none");
  changeClassName("img", "img_pause_animation");
  addClassName("wave_animation", "opacity-0");
  audio.pause();
}

function reloadAudio() {
  addClassName("play", "d-none");
  removeClassList("pause", "d-none");
  changeClassName("img", "img_play_animation");
  removeClassList("wave_animation", "d-none");
  audio.load();
  audio.play();
}

function adjustVolume() {
  var input_number = dom_id_value("adjust_volume") * 0.01;
  audio.volume = input_number;
}

function changeCurrentTime() {
  var timer = dom_id("timer");
  var input_time = timer.value / 100;
  var audio_length = Math.floor(audio.duration);
  current_time = input_time * audio_length;
  audio.currentTime = current_time;
}

function playPrevAudio() {
  randomBackgroundColor();
  if (isShuffle) {
    randomCount();
  } else {
    count--;
  }
  if (count === -1) {
    audio_seri.innerHTML = `Playing music ${Album.seri[4]} of ${Album.seri.length}`;
    audio_name.innerHTML = Album.name[4];
    artist_name.innerHTML = Album.artist[4];
    audio_path.src = Album.path[4];
    audio_image.src = Album.image[4];
    playAudio();
    return (count = 4);
  } else {
    audio_seri.innerHTML = `Playing music ${Album.seri[count]} of ${Album.seri.length}`;
    audio_name.innerHTML = Album.name[count];
    artist_name.innerHTML = Album.artist[count];
    audio_path.src = Album.path[count];
    audio_image.src = Album.image[count];
    playAudio();
  }
}

function playNextAudio() {
  randomBackgroundColor();
  if (isShuffle) {
    randomCount();
  } else {
    count++;
  }
  if (count === 5) {
    audio_seri.innerHTML = `Playing music ${Album.seri[0]} of ${Album.seri.length}`;
    audio_name.innerHTML = Album.name[0];
    artist_name.innerHTML = Album.artist[0];
    audio_path.src = Album.path[0];
    audio_image.src = Album.image[0];
    playAudio();
    return (count = 0);
  } else {
    audio_seri.innerHTML = `Playing music ${Album.seri[count]} of ${Album.seri.length}`;
    audio_name.innerHTML = Album.name[count];
    artist_name.innerHTML = Album.artist[count];
    audio_path.src = Album.path[count];
    audio_image.src = Album.image[count];
    playAudio();
  }
}

function randomCount() {
  return (count = Math.floor(Math.random() * 5));
}

function toggleShuffle() {
  if (isShuffle) {
    removeClassList("shuffle_btn", "text-danger");
    return (isShuffle = !isShuffle);
  } else {
    addClassName("shuffle_btn", "text-danger");
    return (isShuffle = !isShuffle);
  }
}

function autoPlayNextSong() {
  if (audio.ended) {
    playNextAudio();
  }
}

function myTimer() {
  var cur_time = audio.currentTime;
  var cur_length = audio.duration;
  dom_id("timer").value = (100 * cur_time) / cur_length;
  var minute_part = Math.floor(audio.duration / 60);
  var second_part = Math.floor(audio.duration % 60);
  if (second_part < 10) {
    full_time_text.innerHTML = `0${minute_part}:0${second_part}`;
  } else {
    full_time_text.innerHTML = `0${minute_part}:${second_part}`;
  }
  if (audio.currentTime < 60) {
    if (audio.currentTime < 10) {
      played_time_text.innerHTML = `0:0${Math.floor(audio.currentTime)}`;
    } else {
      played_time_text.innerHTML = `0:${Math.floor(audio.currentTime)}`;
    }
  } else {
    c = Math.floor(audio.currentTime / 60);
    d = Math.floor(audio.currentTime % 60);
    if (d < 10) {
      played_time_text.innerHTML = `0${c}:0${d}`;
    } else {
      played_time_text.innerHTML = `0${c}:${d}`;
    }
  }
  autoPlayNextSong();
}

setInterval(myTimer, 1000);
